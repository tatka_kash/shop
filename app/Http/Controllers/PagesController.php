<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index()
    {
        $data['products'] = DB::table('products')->get();
        $data['orders'] = DB::table('orders')->get();
        $data['pages'] = DB::table('pages')->get();
        return view('index', $data);
    }

}
