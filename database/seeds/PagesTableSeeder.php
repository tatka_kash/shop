<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
                array(
                    'title' => 'Denim',
                    'alias' => 'denim',
                    'intro' => 'Parade your pins in a pair of shorts',
                    'content' => 'A versatile day-to-night option, new season shorts aren\'t all about showing some leg - think mid-length culottes and borrowed-from-the-boy longline styles for a more conservative take on the cropped length.'
                ),
                array(
                    'title' => 'Tops',
                    'alias' => 'tops',
                    'intro' => 'Steal the style top spot in a statement separate from the tops collection',
                    'content' => 'Hit refresh on your jersey basics with pastel hues and pick a quirky kimono to give your ensemble that Eastern-inspired edge.'
                ),
                array(
                    'title' => 'Bodysuits',
                    'alias' => 'bodysuits',
                    'intro' => 'Off the shoulder styles are oh-so-sweet, with slogans making your tee a talking point.',
                    'content' => 'Camis or crops, bandeaus or bralets, we have got all the trend-setting tops so you can stay statement in separates this season.'
                )]
        );
    }
}
