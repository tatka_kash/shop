<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
                array(
                    'customer_name' => 'Sofia',
                    'email' => 'sofia88@gmail.com',
                    'phone' => '165-15-40',
                    'feedback' => 'Very comfortable and beautiful dress.'
                ),
                array(
                    'customer_name' => 'Jane',
                    'email' => 'jane85@gmail.com',
                    'phone' => '165-88-14',
                    'feedback' => 'Good service and fast delivery.'
                ),
                array(
                    'customer_name' => 'Gertruda',
                    'email' => 'gertruda91@gmail.com',
                    'phone' => '181-78-19',
                    'feedback' => 'Thank you for good quality of clothes.'
                )]
        );
    }
}
