<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
                array(
                    'title' => 'Georgie Gingham Tie Front Skater Dress',
                    'alias' => 'georgie-gingham-tie-front-skater-dress',
                    'price' => '15',
                    'description' => 'From cool-tone whites to block brights, we have got the everyday skater dresses and party-ready bodycon styles that are perfect for transitioning from day to play.'
                ),
                array(
                    'title' => 'Meg Cap Sleeve Wrap Midi Dress',
                    'alias' => 'meg-cap-sleeve-wrap-midi-dress',
                    'price' => '18',
                    'description' => 'Dresses are the most-wanted wardrobe item for day-to-night dressing.'
                ),
                array(
                    'title' => 'Lilly Crochet Trim Sundress',
                    'alias' => 'lilly-crochet-trim-sundress',
                    'price' => '18',
                    'description' => 'Minis, midis and maxis are our motto, with classic jersey always genius and printed cami dresses the season\'s killer cut - add skyscraper heels for a seriously statement look.'
                )]
        );
    }
}
