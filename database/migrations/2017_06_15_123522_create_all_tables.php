<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('alias');
            $table->text('intro');
            $table->text('content');
            $table->timestamps();
        });
        Schema::create('orders', function (Blueprint $table){
            $table->increments('id');
            $table->string('customer_name');
            $table->text('email');
            $table->text('phone');
            $table->text('feedback');
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('alias');
            $table->text('price');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
