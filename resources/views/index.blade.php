@extends('layouts.layout')

@section('content')
    <div class="row">
        <h2>Products:</h2>
        @foreach($products AS $product)
        <div class="col-md-12">
            <h3>{{$product->title}}</h3>
            <h4>Price: {{$product->price}}</h4>
            <p>{{$product->description}}</p>
        </div>
        @endforeach
    </div>
    <div class="row">
        <h2>Orders:</h2>
        @foreach($orders AS $order)
            <div class="col-md-12">
                <h3>{{$order->customer_name}}</h3>
                <p>E-mail: {{$order->email}}</p>
                <p>Phone: {{$order->phone}}</p>
                <p>Feedback: {{$order->feedback}}</p>
            </div>
        @endforeach
    </div>
    <div class="row">
        <h2>Pages:</h2>
        @foreach($pages AS $page)
            <div class="col-md-12">
                <h3>{{$page->title}}</h3>
                <p>Intro: {{$page->intro}}</p>
                <p>Content: {{$page->content}}</p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Shop</h1>
    </div>
@endsection